/*
 * SPDX-FileCopyrightText: 2021 Sai Moukthik Konduru <saimoukthik777@gmail.com>
 * SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>

 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "todolistmodel.h"
#include "todoitemmodel.h"
#include <QSettings>
#include <QJsonArray>
#include <QJsonDocument>

ToDoListModel::ToDoListModel(QObject *parent)
    : QAbstractListModel(parent)
{
    m_settings = new QSettings(parent);
    load();
}

ToDoListModel::~ToDoListModel()
{
    save();
}

int ToDoListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_Lists.count();
}

QVariant ToDoListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto *list = m_Lists.at(index.row());

    if(role == Roles::ToDoListRole)
        return QVariant::fromValue(list);

    return {};
}

Qt::ItemFlags ToDoListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> ToDoListModel::roleNames() const
{
    return {{Roles::ToDoListRole, "todoList"}};
}

void ToDoListModel::save()
{
    QJsonArray array;
    int i = 0;
    while(i< m_Lists.size()){
        QJsonValue temp;
        temp = m_Lists[i]->toJson();
        array.append(temp);
        i++;
    }
    m_settings->setValue("lists", QString(QJsonDocument(array).toJson(QJsonDocument::Compact)));
    m_settings->sync();
}

void ToDoListModel::load()
{
    QJsonDocument doc = QJsonDocument::fromJson(m_settings->value(QStringLiteral("lists")).toString().toUtf8());
    const auto array = doc.array();

    for(int i = 0; i<array.size(); i++){
        m_Lists.append(new ToDoList(array[i].toObject()));
    }

}

QString name_generator(int n){
    QString gen_name = "Tasks " + QString::number(n+1);
    return gen_name;
}

void ToDoListModel::appendList()
{
    beginInsertRows({}, m_Lists.count(), m_Lists.count());

    ToDoList* list = new ToDoList(this, name_generator(rowCount()), new ToDoItemModel(this));
    m_Lists.append(list);
    emit endInsertRows();
}

void ToDoListModel::removeSelectedList(int index)
{

    beginRemoveRows({}, index, index);
    m_Lists.removeAt(index);
    emit endRemoveRows();

}
