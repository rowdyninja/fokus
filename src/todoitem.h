/*
 * SPDX-FileCopyrightText: 2021 Sai Moukthik Konduru <saimoukthik777@gmail.com>
 * SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>

 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef TODOITEM_H
#define TODOITEM_H

#include <QObject>
#include <QJsonObject>

class ToDoItem : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool done READ done WRITE setDone NOTIFY propertyChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY propertyChanged)
    Q_PROPERTY(bool priority READ priority WRITE setPriority NOTIFY propertyChanged)

public:
    explicit ToDoItem(QObject *parent = nullptr, bool done = false, QString description = "", bool priority = false);
    explicit ToDoItem(const QJsonObject &object);

    QJsonObject toJson() const;

    bool done() const
    {
        return m_done;
    }

    QString description() const
    {
        return m_description;
    }

    bool priority() const
    {
        return m_priority;
    }

    void setDone(const bool &done);
    void setDescription(const QString &description);
    void setPriority(const bool &priority);

private:
    bool m_done;
    QString m_description;
    bool m_priority;

signals:
    void propertyChanged();

};

#endif // TODOITEM_H
