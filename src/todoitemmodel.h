/*
 * SPDX-FileCopyrightText: 2021 Sai Moukthik Konduru <saimoukthik777@gmail.com>
 * SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>

 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef TODOITEMMODEL_H
#define TODOITEMMODEL_H

#include <QAbstractListModel>
#include "todoitem.h"
#include <QJsonArray>

class ToDoItem;

class ToDoItemModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ToDoItemModel(QObject *parent = nullptr);
    explicit ToDoItemModel(const QJsonArray &obj);

    QJsonArray toJson() const;

    enum Roles{
        ToDoItemRole = Qt::UserRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    QHash<int, QByteArray> roleNames() const override;

public slots:
    void append();
    void removeCompletedItem();
    void updateItem(int index); // call UI update for item
    ToDoItem* get(const int& index) const;
    void move(int oldIndex, int newIndex);
    int completedIndex();

private:
    QVector<ToDoItem*> m_Items;
};

#endif // TODOITEMMODEL_H

