﻿/*
 * SPDX-FileCopyrightText: 2021 Sai Moukthik Konduru <saimoukthik777@gmail.com>
 * SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>

 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.1
import org.kde.kirigami 2.10 as Kirigami
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.3
import ToDo 1.0

Kirigami.ScrollablePage{
    id : root
    property alias todoList: list.model
    property ToDoItem currentItem

    ListView {
        id:list
        currentIndex: -1

        property var collapsed: ({})

        delegate: Kirigami.DelegateRecycler{

            property bool expanded: list.isSectionExpanded(model.todoItem.done)

            height: expanded ? implicitHeight : 0
            width: parent ? parent.width : implicitWidth

            Behavior on height {
                NumberAnimation{
                    duration: 200
                }
            }

            sourceComponent: delegateComponent
        }
        Component{
            id: delegateComponent

            Kirigami.BasicListItem {

                id: listItem

                clip: true

                onClicked: {
                    list.currentIndex = index
                    editDialogDescription.text = model.todoItem.description
                    currentItem = model.todoItem
                    editDescriptionDialog.open()
                }

                RowLayout {
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    Kirigami.ListItemDragHandle {
                        listItem: listItem
                        listView: list
                        onMoveRequested:
                        {
                            if(!model.todoItem.done && newIndex < todoList.completedIndex()) {
                                todoList.move(oldIndex, newIndex)
                            }
                        }
                    }


                    Controls.CheckBox {
                        Layout.alignment: Qt.AlignLeft
                        checked: model.todoItem.done
                        onClicked: {
                            let completedIndex = todoList.completedIndex();
                            model.todoItem.done = checked
                            if (model.todoItem.done) {
                                todoList.move(index, completedIndex-1)
                            } else {
                                todoList.move(index, 0)
                            }
                        }
                    }

                    Controls.Label {
                        id: task
                        Layout.alignment: Qt.AlignLeft
                        Layout.fillWidth: true
                        font.strikeout: model.todoItem.done
                        font.bold: model.todoItem.priority
                        text: model.todoItem.description
                        wrapMode: Text.Wrap
                        font.pointSize: 15
                    }
                }
            }
        }

        Kirigami.OverlaySheet {
            id: editDescriptionDialog

            footer: RowLayout {
                Item {
                    Layout.fillWidth: true
                }

                Controls.Switch {
                    checked: currentItem.priority
                    onClicked: {
                        currentItem.priority = currentItem.priority ? false : true
                    }
                }

                Controls.Button {
                    flat: false
                    text: "Cancel"
                    Layout.alignment: Qt.AlignRight
                    onClicked: editDescriptionDialog.close()
                }
                Controls.Button {
                    flat: false
                    text: "Done"
                    onClicked: {
                        currentItem.description = editDialogDescription.text
                        editDescriptionDialog.close()
                    }
                    Layout.alignment: Qt.AlignRight
                }
            }

            GridLayout {
                columns: 2
                rowSpacing: Kirigami.Units.largeSpacing
                Kirigami.Heading {
                    text: i18n("Task")
                    level: 4
                }
                Controls.TextField {
                    id: editDialogDescription
                }
            }
        }

        section{
            id: section
            property: "todoItem.done"
            criteria: ViewSection.FullString

            delegate: Component {
                id: sectionHeading
                Rectangle {
                    width: parent.width
                    height: heading.height + Kirigami.Units.largeSpacing * 2
                    color: "#DDDDDD"

                    signal clicked()

                    Kirigami.Heading {
                        id: heading
                        text: section == "true" ? "Completed" : "To Do"
                        level: 1

                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: Kirigami.Units.largeSpacing
                    }

                    MouseArea{
                        anchors.fill: parent
                        onClicked: parent.clicked()
                    }

                    onClicked: list.toggleSection( section )
                }

            }
        }
        function isSectionExpanded(section){
            return !(section in collapsed)
        }

        function showSection(section){
            delete collapsed[section]
            collapsedChanged();
        }

        function hideSection(section){
            collapsed[section] = true
            collapsedChanged();
        }

        function toggleSection(section){
            if (isSectionExpanded(section)) {
                hideSection(section)
            } else {
                showSection(section)
            }
        }
    }

    actions {
        left: Kirigami.Action {
            icon.name:"list-add"
            onTriggered: {
                todoList.append()
                if(todoList.rowCount() - todoList.completedIndex() > 0){
                    todoList.move(todoList.rowCount()-1, todoList.completedIndex())
                }

                editDialogDescription.text = ""
                currentItem = todoList.get(todoList.completedIndex()-1)
                editDescriptionDialog.open()
            }
        }
        right: Kirigami.Action{
            icon.name: "list-remove"
            onTriggered: {
                todoList.removeCompletedItem()
            }
        }
    }
}
