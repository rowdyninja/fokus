/*
 * SPDX-FileCopyrightText: 2021 Sai Moukthik Konduru <saimoukthik777@gmail.com>
 * SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>

 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "todolist.h"
#include "todoitemmodel.h"
#include <QJsonValue>

ToDoList::ToDoList(QObject *parent, QString name, ToDoItemModel* list) : QObject(parent)
  ,m_name(name)
  ,m_list(list)
{}

ToDoList::ToDoList(const QJsonObject &obj)
{
    m_list = new ToDoItemModel(obj["list"].toArray());
    m_name = obj["name"].toString();
}

QJsonObject ToDoList::toJson() const
{
    QJsonObject obj;
    obj["name"] = m_name;
    obj["list"] = QJsonValue(m_list->toJson());
    return obj;
}

void ToDoList::setName(const QString &name)
{
    m_name = name;
    emit propertyChanged();
}

void ToDoList::setList(ToDoItemModel * &list)
{
    m_list = list;
    emit propertyChanged();
}
