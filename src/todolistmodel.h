/*
 * SPDX-FileCopyrightText: 2021 Sai Moukthik Konduru <saimoukthik777@gmail.com>
 * SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>

 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef TODOLISTMODEL_H
#define TODOLISTMODEL_H

#include <QAbstractListModel>
#include "todolist.h"
#include <QSettings>

class ToDoItemModel;

class ToDoListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ToDoListModel(QObject *parent = nullptr);
    ~ToDoListModel();

    enum Roles{
        ToDoListRole = Qt::UserRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    QHash<int, QByteArray> roleNames() const override;

    void save();
    void load();

public slots:
    void appendList();
    void removeSelectedList(int index);

private:
    QVector<ToDoList*> m_Lists;
    QSettings * m_settings;
};

#endif // TODOLISTMODEL_H
