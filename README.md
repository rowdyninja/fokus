Fokus is a convergent task management app built using [Kirigami](https://develop.kde.org/frameworks/kirigami//) and [Qt](https://www.qt.io/) frameworks. Fokus is mainly being built for plasma-mobile platform, but it can also be used on the desktop.

Features
---
* Simple and elegant UI.
* Ability to quickly add and remove tasks to a todo list.
* Ability to drag and reorder the tasks.
* Ability to create multiple todo lists.

Links
---
* Project page: https://invent.kde.org/rowdyninja/fokus
* Author: @rowdyninja:kde.org (matrix id)

Installing
---
`cmake -B build/ . && cmake --build build/`
